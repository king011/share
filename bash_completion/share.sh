#!/bin/bash
#Program:
#       go share for bash completion
#History:
#       2018-12-04 king011 first release
#Email:
#       zuiwuchang@gmail.com

function __king011_share_daemon()
{
    # 參數定義
    local opts='-c --configure -h --help'
    case ${COMP_WORDS[COMP_CWORD-1]} in
        -c|--configure)
           _filedir || COMPREPLY=( $(compgen -o plusdirs -f ${cur}) )
        ;;

        # default
        *)
            COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
        ;;
    esac
}
function __king011_share_ls()
{
    # 參數定義
    local opts='-a --all -l --long -r --recursive -c --configure -h --help'
    case ${COMP_WORDS[COMP_CWORD-1]} in
        -c|--configure)
           _filedir || COMPREPLY=( $(compgen -o plusdirs -f ${cur}) )
        ;;

        # default
        *)
            COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
        ;;
    esac
}
function __king011_share_get()
{
    # 參數定義
    local opts='-o --output -c --configure -h --help'
    case ${COMP_WORDS[COMP_CWORD-1]} in
        -c|--configure|-o|--output)
           _filedir || COMPREPLY=( $(compgen -o plusdirs -f ${cur}) )
        ;;

        # default
        *)
            COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
        ;;
    esac
}
function __king011_share_sync()
{
    # 參數定義
    local opts='-t --test -k --keep -o --output -c --configure -h --help'
    case ${COMP_WORDS[COMP_CWORD-1]} in
        -c|--configure|-o|--output)
           _filedir || COMPREPLY=( $(compgen -o plusdirs -f ${cur}) )
        ;;

        -k|--keep)
           COMPREPLY=()
        ;;

        # default
        *)
            COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
        ;;
    esac
}
function __king011_share()
{
    # 獲取 正在輸入的 參數
    local cur=${COMP_WORDS[COMP_CWORD]}
    
    #  輸入 第1個 參數
    if [ 1 == $COMP_CWORD ];then
        local opts="daemon get ls sync help --help -h -v --version"
        COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
    else
        # switch 子命令
        case ${COMP_WORDS[1]} in
            daemon)
                __king011_share_daemon
            ;;

            ls)
               __king011_share_ls
            ;;
            
            get)
               __king011_share_get
            ;;

            sync)
               __king011_share_sync
            ;;
        esac
    fi
}

complete -F __king011_share share
complete -F __king011_share ./share