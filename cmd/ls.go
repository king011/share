package cmd

import (
	"context"
	"fmt"
	"io"
	"log"
	"os"
	"path"
	"strings"

	"github.com/spf13/cobra"
	grpc_system "gitlab.com/king011/share/protocol/system"
	"google.golang.org/grpc"
)

func init() {
	var filename string
	var l, a, recursive bool
	cmd := &cobra.Command{
		Use:   "ls",
		Short: "list files",
		Run: func(cmd *cobra.Command, args []string) {
			c, e := NewClient(getClientFilename(filename))
			if e != nil {
				log.Fatalln(e)
			}
			defer c.Close()
			for i := 0; i < len(args); i++ {
				dir := strings.TrimSpace(args[i])
				if dir != "" {
					list(c, dir, recursive, l, a)
				}
			}
		},
	}
	flags := cmd.Flags()
	flags.StringVarP(&filename,
		"configure", "c",
		"",
		"configure file path",
	)
	flags.BoolVarP(&l,
		"long", "l",
		false,
		"use a long listing format",
	)
	flags.BoolVarP(&a,
		"all", "a",
		false,
		"do not ignore entries starting with .",
	)
	flags.BoolVarP(&recursive,
		"recursive", "r",
		false,
		"if ture recursive list child directory",
	)

	rootCmd.AddCommand(cmd)
}
func list(cc *grpc.ClientConn, dir string, recursive, l, a bool) {
	c := grpc_system.NewSystemClient(cc)
	stream, e := c.List(context.Background())
	if e != nil {
		log.Fatalln(e)
	}
	e = stream.Send(&grpc_system.ListRequest{
		Dir:       dir,
		Recursive: recursive,
	})
	if e != nil {
		log.Fatalln(e)
	}

	for {
		var response *grpc_system.ListResponse
		response, e = stream.Recv()
		if e != nil {
			if e == io.EOF {
				break
			}
			log.Fatalln(e)
		}
		var line bool
		for i := 0; i < len(response.Data); i++ {
			node := response.Data[i]

			if !a && strings.HasPrefix(path.Base(node.Path), ".") {
				continue
			}

			if l {
				fmt.Printf("%v %-20v %v\n", os.FileMode(node.Mode), getSizeStr(node.Size), node.Path)
			} else {
				if recursive {
					fmt.Println(node.Path)
				} else {
					fmt.Print(path.Base(node.Path), "   ")
					line = true
				}
			}
		}
		if line {
			fmt.Println()
		}
	}
}
