package cmd

import (
	"fmt"
	"log"
	"os"
	"path"
	"path/filepath"
	"regexp"
	"strings"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/king011/king-go/os/fileperm"
	"google.golang.org/grpc"
)

func init() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	var filename string
	var output string
	var keep []string
	var test bool
	cmd := &cobra.Command{
		Use:   "sync",
		Short: "same as get but if files not exist on server remove local files",
		Long: `same as get but if files not exist on server remove local files

  # sync directory /src and not remove directory /bin or /pkg
  sync /src -k ^/bin -k ^/pkg
  sync /cmd -k "^/(bin)|(pkg)"

  # sync and rename
  sync /cmd:/command -k "^/(bin)|(pkg)"


  # test sync and print remove list
  sync /src -k ^/bin -k ^/pkg -t
  sync /cmd -k "^/(bin)|(pkg) -t"
`,
		Run: func(cmd *cobra.Command, args []string) {
			begin := time.Now()
			// 創建 正則
			keys := make(map[string]bool)
			match := make([]*regexp.Regexp, 0, len(keep))
			for i := 0; i < len(keep); i++ {
				str := keep[i]
				if str == "" || keys[str] {
					continue
				}
				keys[str] = true
				r, e := regexp.Compile(str)
				if e != nil {
					log.Fatalln(e)
				}
				match = append(match, r)
			}

			output = strings.TrimSpace(output)
			if output == "" {
				output = "output"
			}
			var e error
			output, e = filepath.Abs(output)
			if e != nil {
				log.Fatalln(e)
			}
			e = os.MkdirAll(output, fileperm.Directory)
			if e != nil {
				log.Fatalln(e)
			}
			fmt.Println("sync output", output)

			c, e := NewClient(getClientFilename(filename))
			if e != nil {
				log.Fatalln(e)
			}
			defer c.Close()
			for i := 0; i < len(args); i++ {
				filename := strings.TrimSpace(args[i])
				if filename != "" {
					syncFile(c, test, filename, output, match)
				}
			}
			fmt.Println("complete", time.Now().Sub(begin))
		},
	}
	flags := cmd.Flags()
	flags.StringVarP(&filename,
		"configure", "c",
		"",
		"configure file path",
	)
	flags.StringVarP(&output,
		"output", "o",
		"",
		"output directory",
	)
	flags.StringSliceVarP(&keep,
		"keep", "k",
		nil,
		"if path match keep not remove",
	)
	flags.BoolVarP(&test,
		"test", "t",
		false,
		"test sync and print remove list, not write disk or download",
	)
	rootCmd.AddCommand(cmd)
}

func syncFile(cc *grpc.ClientConn, test bool, filename, output string, match []*regexp.Regexp) {
	if !strings.HasPrefix(filename, "/") {
		log.Fatalln("filename must start width /")
	}
	strs := strings.SplitN(filename, ":", 2)
	filename = path.Clean(strs[0])
	var dst string
	if len(strs) == 2 {
		dst = path.Clean(strs[1])
		if !strings.HasPrefix(dst, "/") {
			log.Fatalln("dst must start width /")
		}
	} else {
		dst = filename
	}

	// 返回 下載列表
	files := getList(cc, filename)
	if test {
		sum := 0
		for i := 0; i < len(files); i++ {
			if !files[i].Dir {
				sum++
			}
		}
		fmt.Printf("download %v files\n", sum)
	}

	// 轉化 本地 目標 名稱
	infos := getTransform(filename, dst, output, files)

	// 刪除 不存在的 檔案
	syncRemove(test, output, filename, dst, infos, match)
	if test {
		return
	}
	// 執行下載
	getDone(cc, infos)
}

func syncRemove(test bool, output, filename, dst string, files []FileInfo, match []*regexp.Regexp) {
	keys := make(map[string]*FileInfo)
	if filename == dst {
		for i := 0; i < len(files); i++ {
			keys[files[i].Path] = &files[i]
		}
	} else {
		for i := 0; i < len(files); i++ {
			key := path.Clean(strings.Replace(files[i].Path, filename, dst, 1))
			keys[key] = &files[i]
		}
	}
	n := len(output)
	outputRoot := output + dst
	filepath.Walk(output, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			if os.IsNotExist(err) {
				return nil
			}
			log.Fatalln(err)
		}
		if !strings.HasPrefix(path, outputRoot) {
			return nil
		}
		root := path[n:]
		if find, ok := keys[root]; ok && find.Dir == info.IsDir() {
			return nil
		}
		for i := 0; i < len(match); i++ {
			if match[i].MatchString(root) {
				return nil
			}
		}
		if test {
			fmt.Println("rm", root)
		} else {
			err = os.RemoveAll(path)
			if err != nil {
				log.Fatalln(err)
			}
		}
		return nil
	})
}
