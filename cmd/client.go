package cmd

import (
	"context"
	"crypto/sha512"
	"crypto/tls"
	"encoding/hex"
	"gitlab.com/king011/share/configure"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

// Token 自定義 token 結構
type Token struct {
	safe bool
	m    map[string]string
}

// NewToken 返回 一個 token
func NewToken(safe bool, m map[string]string) credentials.PerRPCCredentials {
	return Token{
		safe: safe,
		m:    m,
	}
}

// RequireTransportSecurity 返回 是否 需要 安全傳輸 token
// 如果 返回 true 則必須 使用 WithTransportCredentials 創建 客戶端 (既安全的 加密通道)
func (t Token) RequireTransportSecurity() bool {
	return t.safe
}

// GetRequestMetadata 返回 token 值
func (t Token) GetRequestMetadata(ctx context.Context, uri ...string) (map[string]string, error) {
	//返回 token
	return t.m, nil
}

// NewClient 創建 grpc 客戶端
func NewClient(filename string) (c *grpc.ClientConn, e error) {
	var cnf configure.Client
	e = cnf.Load(filename)
	if e != nil {
		return
	}
	m := make(map[string]string)
	m["name"] = cnf.User
	data := sha512.Sum512([]byte(cnf.Password))
	m["pwd"] = hex.EncodeToString(data[:])
	token := NewToken(cnf.TLS, m)

	if cnf.TLS {
		var creds credentials.TransportCredentials
		if cnf.SkipVerify {
			creds = credentials.NewTLS(&tls.Config{
				InsecureSkipVerify: true,
			})
		} else {
			creds = credentials.NewTLS(&tls.Config{})
		}

		c, e = grpc.Dial(
			cnf.Addr,
			grpc.WithTransportCredentials(creds),
			grpc.WithPerRPCCredentials(token),
		)
	} else {
		c, e = grpc.Dial(
			cnf.Addr,
			grpc.WithInsecure(),
			grpc.WithPerRPCCredentials(token),
		)
	}
	return
}
