package cmd

import (
	"context"
	"fmt"
	"hash/crc32"
	"io"
	"log"
	"os"
	"path"
	"path/filepath"
	"strings"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/king011/king-go/os/fileperm"
	grpc_data "gitlab.com/king011/share/protocol/data"
	grpc_system "gitlab.com/king011/share/protocol/system"
	"gitlab.com/king011/share/speed"
	"google.golang.org/grpc"
)

func init() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	var filename string
	var output string
	cmd := &cobra.Command{
		Use:   "get",
		Short: "get files from server and download",
		Long: `get files from server and download
		
  # download
  get /go/src /go/bin

  # download and rename
  get /go/src:/src /go/bin:/bin
`,
		Run: func(cmd *cobra.Command, args []string) {
			begin := time.Now()
			output = strings.TrimSpace(output)
			if output == "" {
				output = "output"
			}
			var e error
			output, e = filepath.Abs(output)
			if e != nil {
				log.Fatalln(e)
			}
			e = os.MkdirAll(output, fileperm.Directory)
			if e != nil {
				log.Fatalln(e)
			}
			fmt.Println("get output", output)

			c, e := NewClient(getClientFilename(filename))
			if e != nil {
				log.Fatalln(e)
			}
			defer c.Close()
			for i := 0; i < len(args); i++ {
				filename := strings.TrimSpace(args[i])
				if filename != "" {
					get(c, filename, output)
				}
			}
			fmt.Println("complete", time.Now().Sub(begin))
		},
	}
	flags := cmd.Flags()
	flags.StringVarP(&filename,
		"configure", "c",
		"",
		"configure file path",
	)
	flags.StringVarP(&output,
		"output", "o",
		"",
		"output directory",
	)

	rootCmd.AddCommand(cmd)
}
func get(cc *grpc.ClientConn, filename, output string) {
	if !strings.HasPrefix(filename, "/") {
		log.Fatalln("filename must start width /")
	}
	strs := strings.SplitN(filename, ":", 2)
	filename = path.Clean(strs[0])
	var dst string
	if len(strs) == 2 {
		dst = path.Clean(strs[1])
		if !strings.HasPrefix(dst, "/") {
			log.Fatalln("dst must start width /")
		}
	} else {
		dst = filename
	}

	// 返回 下載列表
	files := getList(cc, filename)
	if len(files) == 0 {
		return
	}
	// 轉化 本地 目標 名稱
	infos := getTransform(filename, dst, output, files)
	// 執行下載
	getDone(cc, infos)
}

// FileInfo 檔案信息
type FileInfo struct {
	// 相對 根路徑的 path
	Path string
	// 是否是 檔案夾
	Dir  bool
	Mode os.FileMode
	// 檔案大小
	Size int64

	// 本地 目標名稱
	Dst string
}

func getTransform(filename, dst, output string, files []*grpc_data.FileInfo) (infos []FileInfo) {
	infos = make([]FileInfo, len(files))
	if filename == dst {
		for i, node := range files {
			infos[i].Path = node.Path
			infos[i].Dir = node.Dir
			infos[i].Mode = os.FileMode(node.Mode)
			infos[i].Size = node.Size

			infos[i].Dst = output + node.Path
			infos[i].Dst = path.Clean(infos[i].Dst)
		}
	} else {
		for i, node := range files {
			infos[i].Path = node.Path
			infos[i].Dir = node.Dir
			infos[i].Mode = os.FileMode(node.Mode)
			infos[i].Size = node.Size

			infos[i].Dst = output + strings.Replace(node.Path, filename, dst, 1)
			infos[i].Dst = path.Clean(infos[i].Dst)
		}
	}
	return
}
func getDone(cc *grpc.ClientConn, files []FileInfo) {
	// 計算 hash
	last, items := getItems(0, files)
	Rprintf(last, "download %v files\n", len(items))
	last = 0

	// 創建 檔案夾
	index := 0
	last, index = getMkDir(files, index)

	// 下載 檔案
	var total int64
	for i := 0; i < len(files); i++ {
		if !files[i].Dir {
			total += files[i].Size
		}
	}
	last = getDownload(cc, items, index, files, total)

	// 修改 mode
	for i := 0; i < len(files); i++ {
		dst := files[i].Dst
		e := os.Chmod(dst, os.FileMode(files[i].Mode))
		if e != nil {
			if last != 0 {
				fmt.Println()
			}
			log.Fatalln(e)
		}

	}

	if last != 0 {
		fmt.Println()
	}
}
func getDownload(cc *grpc.ClientConn, items []*grpc_system.GetNode, index int, files []FileInfo, total int64) (last int) {
	c := grpc_system.NewSystemClient(cc)
	stream, e := c.Get(context.Background())
	if e != nil {
		log.Fatalln(e)
	}
	statistics := speed.NewStatistics(time.Second * 5)
	count := len(files)
	var pos int
	var dst string
	for len(items) != 0 {
		n := 256
		if n > len(items) {
			n = len(items)
		}

		// 發送 請求
		e = stream.Send(&grpc_system.GetRequest{
			Items: items[:n],
		})
		if e != nil {
			if last != 0 {
				fmt.Println()
			}
			log.Fatalln(e)
		}

		// 處理 響應
		for i := 0; i < n; i++ {
			index++
			dst, pos = getDst(files, pos)
			last = getDownloadRecv(stream, statistics, dst, items[i], index, count, last, &total)
		}
		items = items[n:]
	}
	return
}
func getDst(files []FileInfo, pos int) (string, int) {
	for ; pos < len(files); pos++ {
		if !files[pos].Dir {
			dst := files[pos].Dst
			pos++
			return dst, pos
		}
	}
	log.Fatalln("not find dst")
	return "", pos
}
func getSpeed(statistics *speed.Statistics) (str string) {
	s := statistics.Speed()
	if s < 1 {
		return
	}
	str = fmt.Sprintf("[%v/s]", getSizeStr(s))
	return
}
func getETA(statistics *speed.Statistics, total int64) (str string) {
	if total < 1024 {
		return
	}
	s := statistics.Speed()
	if s < 1024 {
		return
	}

	str = fmt.Sprint(time.Duration(total*int64(time.Second)/s), " ETA")
	return
}
func getDownloadRecv(stream grpc_system.System_GetClient, statistics *speed.Statistics, dst string, file *grpc_system.GetNode, index, count, last int, total *int64) int {
	begin := time.Now()

	// 打印當前進度
	last = Rprintf(last, "[%v/%v] %v request %v ",
		index, count, file.Path,
		getETA(statistics, *total),
	)

	// 創建 父檔案夾
	e := os.MkdirAll(path.Dir(dst), fileperm.Directory)
	if e != nil {
		fmt.Println()
		log.Fatalln(e)
	}
	var f *os.File
	defer func() {
		if f != nil {
			f.Close()
		}
		if last != 0 {
			fmt.Println()
			last = 0
		}
	}()

	var response grpc_system.GetResponse
	var totalStr string
	offset := file.Offset
	currentStr := getSizeStr(offset)
	exit := false
	var size int64
	for {
		e = stream.RecvMsg(&response)
		if e != nil {
			fmt.Println()
			log.Fatalln(e)
		}
		switch response.Code {
		case grpc_system.GetResponse_Info:
			size = response.Size
			*total = size
			totalStr = getSizeStr(response.Size)
			last = Rprintf(last, "[%v/%v] %v <%v/%v> %v ",
				index, count, file.Path, currentStr, totalStr,
				getETA(statistics, *total),
			)
		case grpc_system.GetResponse_Changed:
			// 重新 創建 檔案
			f, e = os.Create(dst)
			if e != nil {
				fmt.Println()
				log.Fatalln(e)
			}
			offset = 0
			currentStr = "0"
			last = Rprintf(last, "[%v/%v] %v <%v/%v> %v ",
				index, count, file.Path, currentStr, totalStr,
				getETA(statistics, *total),
			)
		case grpc_system.GetResponse_Bytes:
			if len(response.Data) != 0 {
				// 打開 檔案
				if f == nil {
					f, e = os.OpenFile(dst, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0666)
					if e != nil {
						fmt.Println()
						log.Fatalln(e)
					}
					*total -= offset
				}
				// 寫入 數據
				_, e = f.Write(response.Data)
				if e != nil {
					fmt.Println()
					log.Fatalln(e)
				}
				sum := int64(len(response.Data))
				*total -= sum
				statistics.Push(sum)

				offset += sum
				currentStr = getSizeStr(offset)
				last = Rprintf(last,
					"[%v/%v] %v <%v/%v> %v %v ",
					index, count, file.Path, currentStr, totalStr,
					getSpeed(statistics),
					getETA(statistics, *total),
				)
			}
		case grpc_system.GetResponse_Complete:
			if f == nil && size == 0 {
				// 創建 空 檔案
				f, e = os.Create(dst)
				if e != nil {
					fmt.Println()
					log.Fatalln("unknow code", response.Code)
				}
			}
			exit = true
			last = Rprintf(last,
				"[%v/%v] %v <%v/%v> %v [%v]\n",
				index, count, file.Path, currentStr, totalStr,
				getSpeed(statistics),
				time.Now().Sub(begin),
			)
			last = 0
		default:
			fmt.Println()
			log.Fatalln("unknow code", response.Code)
		}
		if exit {
			break
		}
	}
	return 0
}
func getMkDir(files []FileInfo, index int) (int, int) {
	count := len(files)
	for i := 0; i < count; i++ {
		node := files[i]
		if !node.Dir {
			continue
		}
		index++
		// 打印 當前 進度
		fmt.Printf("[%v/%v] %v \n", index, count, node.Path)
		dst := node.Dst
		info, e := os.Stat(dst)
		if e == nil {
			mode := info.Mode()
			if mode&fileperm.UA != fileperm.UA {
				mode |= fileperm.UA
				e = os.Chmod(dst, mode)
				if e != nil {
					fmt.Println()
					log.Fatalln(e)
				}
			}
		} else {
			if !os.IsNotExist(e) {
				fmt.Println()
				log.Fatalln(e)
			}
			e = os.MkdirAll(dst, fileperm.Directory)
			if e != nil {
				log.Fatalln(e)
			}
		}
	}
	return 0, index
}

func getHash(filename string) (offset int64, hash []byte) {
	// 添加 讀寫 權限
	info, e := os.Stat(filename)
	if e != nil {
		if os.IsNotExist(e) {
			return
		}
		fmt.Println()
		log.Fatalln(e)
	}
	mode := info.Mode()
	if mode&fileperm.URW != fileperm.URW {
		mode |= fileperm.URW
		e = os.Chmod(filename, mode)
		if e != nil {
			fmt.Println()
			log.Fatalln(e)
		}
	}

	// 查詢 檔案 大小
	offset = info.Size()
	if offset == 0 {
		return
	}

	// 計算 hash
	var f *os.File
	f, e = os.Open(filename)
	if e != nil {
		fmt.Println()
		log.Fatalln(e)
	}
	h := crc32.NewIEEE()
	_, e = io.Copy(h, f)
	f.Close()
	if e != nil {
		fmt.Println()
		log.Fatalln(e)
	}
	hash = h.Sum(nil)
	return
}
func getItems(last int, files []FileInfo) (int, []*grpc_system.GetNode) {
	count := len(files)
	if count == 0 {
		return last, nil
	}
	items := make([]*grpc_system.GetNode, 0, count)
	for i := 0; i < count; i++ {
		node := files[i]

		last = Rprintf(last, "hash [%v/%v] %v ", i+1, count, node.Path)
		if !node.Dir {
			offset, hash := getHash(node.Dst)
			items = append(items, &grpc_system.GetNode{
				Path:   node.Path,
				Offset: offset,
				Hash:   hash,
			})
		}
	}
	if len(items) == 0 {
		return last, nil
	}
	return last, items
}
func getList(cc *grpc.ClientConn, filename string) (files []*grpc_data.FileInfo) {
	c := grpc_system.NewSystemClient(cc)
	stream, e := c.List(context.Background())
	if e != nil {
		log.Fatalln(e)
	}
	e = stream.Send(&grpc_system.ListRequest{
		Dir:       filename,
		Recursive: true,
		Root:      true,
	})
	var response grpc_system.ListResponse
	var last int
	for {
		e = stream.RecvMsg(&response)
		if e != nil {
			if e == io.EOF {
				break
			}
			log.Fatalln(e)
		}
		if files == nil {
			files = make([]*grpc_data.FileInfo, 0, len(response.Data))
		}
		for i := 0; i < len(response.Data); i++ {
			files = append(files, response.Data[i])
		}
		last = Rprintf(last, "find %v files", len(files))
	}
	Rprintf(last, "find %v files\n", len(files))
	return
}
