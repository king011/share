package daemon

import (
	"gitlab.com/king011/share/cmd/daemon/server"
	"gitlab.com/king011/share/configure"
	"gitlab.com/king011/share/logger"
	grpc_system "gitlab.com/king011/share/protocol/system"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/reflection"
	"log"
	"net"
)

func runGRPC(cnf *configure.GRPC) (e error) {
	// 創建 tcp Listener
	var l net.Listener
	l, e = net.Listen("tcp", cnf.Addr)
	if e != nil {
		logger.Fatal("listen error",
			zap.Error(e),
		)
		return
	}

	// 創建 grpc 服務器
	var s *grpc.Server
	if cnf.CertFile != "" && cnf.KeyFile != "" {
		var creds credentials.TransportCredentials
		creds, e = credentials.NewServerTLSFromFile(cnf.CertFile, cnf.KeyFile)
		if e != nil {
			logger.Fatal("tls error",
				zap.Error(e),
			)
			return
		}
		s = grpc.NewServer(
			grpc.Creds(creds),
		)
		logger.Info("h2 work",
			zap.String("addr", cnf.Addr),
		)
		log.Println("h2 work at", cnf.Addr)
	} else {
		s = grpc.NewServer()
		logger.Info("h2c work",
			zap.String("addr", cnf.Addr),
		)
		log.Println("h2c work at", cnf.Addr)
	}

	// 註冊 grpc 服務
	grpc_system.RegisterSystemServer(s, server.System{})

	// 註冊路由
	reflection.Register(s)

	// 運行 grpc
	if e := s.Serve(l); e != nil {
		logger.Fatal("run grpc error",
			zap.Error(e),
		)
	}
	return
}
