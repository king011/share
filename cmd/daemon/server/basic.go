package server

import (
	"gitlab.com/king011/share/configure"
	"gitlab.com/king011/share/db/manipulator"
	"golang.org/x/net/context"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
	"path/filepath"
	"strings"
)

// Basic 提供了 通用的 一些 功能
type Basic struct {
}

func (s Basic) getUser(ctx context.Context) (user, pwd string, e error) {
	// 獲取 用戶 信息
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		e = status.Error(codes.Unauthenticated, "unknow token")
		return
	}
	var tmpUser string
	strs := md["name"]
	if len(strs) > 0 {
		tmpUser = strs[0]
	} else {
		e = status.Error(codes.Unauthenticated, "unknow user")
		return
	}
	var tmpPwd string
	strs = md["pwd"]
	if len(strs) > 0 {
		tmpPwd = strs[0]
	} else {
		e = status.Error(codes.Unauthenticated, "unknow pwd")
		return
	}
	user = tmpUser
	pwd = tmpPwd
	return
}

// VerifyRead 驗證 是否 擁有 下載 權限
func (s Basic) VerifyRead(ctx context.Context) (e error) {
	var name, pwd string
	name, pwd, e = s.getUser(ctx)
	if e != nil {
		return
	}
	user := manipulator.SingleUser().Get(name)
	if user == nil || user.Password != pwd {
		e = status.Error(codes.Unauthenticated, "name or pwd not match")
		return
	} else if user.Power&configure.PowerRead == 0 {
		e = status.Error(codes.PermissionDenied, "read permission denied ")
		return
	}
	return
}

// VerifyWrite 驗證 是否 擁有 上傳 權限
func (s Basic) VerifyWrite(ctx context.Context) (e error) {
	var name, pwd string
	name, pwd, e = s.getUser(ctx)
	if e != nil {
		return
	}
	user := manipulator.SingleUser().Get(name)
	if user == nil || user.Password != pwd {
		e = status.Error(codes.Unauthenticated, "name or pwd not match")
		return
	} else if user.Power&configure.PowerWrite == 0 {
		e = status.Error(codes.PermissionDenied, "write permission denied ")
		return
	}
	return
}

// VerifyAdmin 驗證 是否 擁有 管理 權限
func (s Basic) VerifyAdmin(ctx context.Context) (e error) {
	var name, pwd string
	name, pwd, e = s.getUser(ctx)
	if e != nil {
		return
	}
	user := manipulator.SingleUser().Get(name)
	if user == nil || user.Password != pwd {
		e = status.Error(codes.Unauthenticated, "name or pwd not match")
		return
	} else if user.Power&configure.PowerAdmin == 0 {
		e = status.Error(codes.PermissionDenied, "admin permission denied ")
		return
	}
	return
}

// AbsFilename 轉換 到 覺得 路徑
func (s Basic) AbsFilename(filename string) (string, string, error) {
	str := strings.TrimSpace(filename)
	if !strings.HasPrefix(str, "/") {
		return "", "", status.Errorf(codes.InvalidArgument, "unknow filename [ %s ]", filename)
	}

	root := manipulator.Root()
	if filename == "/" {
		str = root
	} else {
		str = root + filename
		str = filepath.Clean(str)
	}
	if !strings.HasPrefix(str, root) {
		return "", "", status.Errorf(codes.PermissionDenied, "[ %v ] permission denied", filename)
	}
	return root, str, nil
}
