package server

import (
	"bytes"
	"hash/crc32"
	"io"
	"os"
	"path/filepath"

	"gitlab.com/king011/share/logger"
	grpc_data "gitlab.com/king011/share/protocol/data"
	grpc_system "gitlab.com/king011/share/protocol/system"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// System 檔案 服務器
type System struct {
	Basic
}

// List 列出 指定檔案夾下的 檔案 不會遞歸子檔案夾
func (s System) List(stream grpc_system.System_ListServer) (e error) {
	// 驗證 權限
	ctx := stream.Context()
	e = s.VerifyRead(ctx)
	if e != nil {
		logger.Warn("list error",
			zap.Error(e),
		)
		return
	}

	var request *grpc_system.ListRequest
	request, e = stream.Recv()
	if e != nil {
		logger.Warn("list error",
			zap.Error(e),
		)
		return
	}
	var root, dir string
	root, dir, e = s.AbsFilename(request.Dir)
	if e != nil {
		logger.Error("list error",
			zap.Error(e),
		)
		return
	}

	n := int(request.Max)
	if n < 32 || n > 1024 {
		n = 256
	}
	items := make([]*grpc_data.FileInfo, 0, n)
	e = filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		isRoot := false
		if err != nil {
			return err
		}
		if info.Mode()&os.ModeSymlink != 0 {
			// 忽略 連接檔案
			return nil
		}
		if dir == path && info.IsDir() {
			isRoot = true
			if !request.Root || root == path {
				return nil
			}
		}
		items = append(items, &grpc_data.FileInfo{
			Path: path[len(root):],
			Dir:  info.IsDir(),
			Mode: uint32(info.Mode()),
			Size: info.Size(),
		})

		if len(items) == n {
			// 返回 響應
			err = stream.Send(&grpc_system.ListResponse{
				Data: items,
			})
			if err != nil {
				logger.Error("list error",
					zap.Error(err),
				)
				return err
			}
			items = items[:0]
		}
		if !isRoot && !request.Recursive && info.IsDir() {
			return filepath.SkipDir
		}
		return nil
	})
	if e != nil || len(items) == 0 {
		return
	}

	e = stream.Send(&grpc_system.ListResponse{
		Data: items,
	})
	if e != nil {
		logger.Error("list error",
			zap.Error(e),
		)
		return
	}
	return
}

// Get 從 服務器 下載 檔案
func (s System) Get(stream grpc_system.System_GetServer) (e error) {
	// 驗證 權限
	ctx := stream.Context()
	e = s.VerifyRead(ctx)
	if e != nil {
		logger.Warn("get error",
			zap.Error(e),
		)
		return
	}

	var request grpc_system.GetRequest
	for {
		// 讀取 請求
		e = stream.RecvMsg(&request)
		if e != nil {
			if grpc.Code(e) != codes.Canceled {
				logger.Error("get error",
					zap.Error(e),
				)
			}
			break
		}
		items := request.Items
		if len(items) == 0 {
			continue
		} else if len(items) > 1024 {
			e = status.Errorf(codes.InvalidArgument, "len(items) == %v, exceed the upper limit.", len(items))
			logger.Error("get error",
				zap.Error(e),
			)
			return
		}
		// 轉換 檔案 路徑
		for _, item := range items {
			if item == nil {
				continue
			}
			var path string
			_, path, e = s.AbsFilename(item.Path)
			if e != nil {
				logger.Error("get error",
					zap.Error(e),
					zap.String("path", item.Path),
				)
				return
			}

			e = s.get(stream, path, item.Offset, item.Hash)
			if e != nil {
				return
			}
		}
	}
	return
}

func (s System) get(stream grpc_system.System_GetServer, path string, offset int64, hash []byte) (e error) {
	// 打開 檔案
	var f *os.File
	defer func() {
		if f != nil {
			f.Close()
		}
	}()
	f, e = os.Open(path)
	if e != nil {
		logger.Error("get error",
			zap.Error(e),
			zap.String("path", path),
		)
		return
	}
	// 返回 檔案信息
	var info os.FileInfo
	info, e = f.Stat()
	if e != nil {
		logger.Error("get error",
			zap.Error(e),
			zap.String("path", path),
		)
		return
	}
	size := info.Size()

	var changed bool
	if offset != 0 {
		if offset > size || len(hash) == 0 {
			changed = true
		} else {
			// 計算 hash
			m := crc32.NewIEEE()
			_, e = io.CopyN(m, f, offset)
			if e != nil {
				logger.Error("get error",
					zap.Error(e),
					zap.String("path", path),
				)
				return
			}
			// 驗證 hash
			if bytes.Compare(hash, m.Sum(nil)) == 0 {
				logger.Debug("get hit",
					zap.Int64("size", offset),
					zap.String("path", path),
				)
			} else {
				logger.Debug("get changed",
					zap.Int64("size", offset),
					zap.String("path", path),
				)
				changed = true
				_, e = f.Seek(0, os.SEEK_SET)
				if e != nil {
					logger.Error("get error",
						zap.Error(e),
						zap.String("path", path),
					)
					return
				}
			}
		}
	}

	// 返回 檔案大小
	var response grpc_system.GetResponse
	response.Code = grpc_system.GetResponse_Info
	response.Size = size
	e = stream.Send(&response)
	if e != nil {
		logger.Error("get error",
			zap.Error(e),
			zap.String("path", path),
		)
		return
	}
	// 緩存 不匹配 通知 重建檔案
	if changed {
		var response grpc_system.GetResponse
		response.Code = grpc_system.GetResponse_Changed
		e = stream.Send(&response)
		if e != nil {
			logger.Error("get error",
				zap.Error(e),
				zap.String("path", path),
			)
			return
		}
	}

	// 返回 檔案數據
	response.Size = 0
	response.Code = grpc_system.GetResponse_Bytes
	b := make([]byte, 1024*16)
	var n int
	for {
		n, e = f.Read(b)
		if e != nil {
			if e == io.EOF {
				e = nil
				break
			}
			logger.Error("get error",
				zap.Error(e),
				zap.String("path", path),
			)
			return
		}
		response.Data = b[:n]
		e = stream.Send(&response)
		if e != nil {
			logger.Error("get error",
				zap.Error(e),
				zap.String("path", path),
			)
			return
		}
	}

	// 通知 檔案 傳輸 完成
	response.Data = nil
	response.Code = grpc_system.GetResponse_Complete
	e = stream.Send(&response)
	if e != nil {
		logger.Error("get error",
			zap.Error(e),
			zap.String("path", path),
		)
		return
	}
	return
}
