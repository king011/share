package daemon

import (
	"gitlab.com/king011/share/configure"
	"gitlab.com/king011/share/db/manipulator"
	"gitlab.com/king011/share/logger"
)

// Run .
func Run(filename string) (e error) {
	// 加載 配置
	var cnf configure.Server
	e = cnf.Load(filename)
	if e != nil {
		return
	}
	e = cnf.Format()
	if e != nil {
		return
	}

	// 初始化 日誌
	logger.Init(&cnf.Logger)

	// 初始化 檔案系統
	e = manipulator.Init(cnf.Root, cnf.User)
	if e != nil {
		return
	}

	// 運行 grpc
	e = runGRPC(&cnf.GRPC)
	return
}
