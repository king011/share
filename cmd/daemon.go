package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/king011/share/cmd/daemon"
	"log"
)

func init() {
	var filename string
	cmd := &cobra.Command{
		Use:   "daemon",
		Short: "run server daemon",
		Run: func(cmd *cobra.Command, args []string) {
			filename = getServerFilename(filename)
			if e := daemon.Run(filename); e != nil {
				log.Fatalln(e)
			}
		},
	}
	flags := cmd.Flags()
	flags.StringVarP(&filename,
		"configure", "c",
		"", "configure file path",
	)
	rootCmd.AddCommand(cmd)
}
