package cmd

import (
	"fmt"
	"os"
	"path"
	"strings"
)

const (
	// KB .
	KB = 1024
	// MB .
	MB = 1024 * KB
	// GB .
	GB = 1024 * MB
	// TB .
	TB = 1024 * GB
)

func getServerFilename(filename string) string {
	filename = strings.TrimSpace(filename)
	if filename == "" {
		filename = path.Dir(os.Args[0]) + "/server.jsonnet"
	}
	return filename
}
func getClientFilename(filename string) string {
	filename = strings.TrimSpace(filename)
	if filename == "" {
		filename = path.Dir(os.Args[0]) + "/client.jsonnet"
	}
	return filename
}
func getSizeStr(size int64) (str string) {
	if size < 1 {
		return
	}

	if size > TB {
		str += fmt.Sprintf("%vT", size/TB)
		size %= TB
	}
	if size > GB {
		str += fmt.Sprintf("%vG", size/GB)
		size %= GB
	}
	if size > MB {
		str += fmt.Sprintf("%vM", size/MB)
		size %= MB
	}
	if size > KB {
		str += fmt.Sprintf("%vK", size/KB)
		size %= KB
	}
	if size != 0 {
		str += fmt.Sprintf("%v", size)
	}
	return
}

// Rprintf \r Printf
func Rprintf(last int, format string, a ...interface{}) int {
	str := fmt.Sprintf(format, a...)
	if last != 0 {
		format := fmt.Sprintf("\r%%-%vs", last)
		fmt.Printf(format, "")
	}
	fmt.Printf("\r%s", str)
	last = len(str)
	return last
}

// Rptint \r Print
func Rptint(last int, a ...interface{}) int {
	str := fmt.Sprint(a...)
	if last != 0 {
		format := fmt.Sprintf("\r%%-%vs", last)
		fmt.Printf(format, "")
	}
	fmt.Printf("\r%s", str)
	last = len(str)
	return last
}
