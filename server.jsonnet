// 允許 下載 檔案
local Read = 1;
// 允許 上傳 檔案
local Write = 2; 
// 允許 管理服務器
local Admin = 4;
local ReadWrite = Read | Write;
local All = Read | Write | Admin;
{
    GRPC:{
        Addr:":17000",
        // 如果爲空 則使用 h2c
        CertFile:"",
        KeyFile:"",
    },
    // 共享 檔案 根目錄
    Root:"./",
    Logger:{
        // 日誌 http 如果爲空 則不啓動 http
        HTTP:"localhost:27000",
        // 日誌 檔案名
        //Filename:"logs/server.log",
        // 單個日誌檔案 大小上限 MB
        //MaxSize:    100, 
        // 保存 多少個 日誌 檔案
        //MaxBackups: 3,
        // 保存 多少天內的 日誌
        //MaxAge:     28,
        // 要 保存的 日誌 等級 debug info warn error dpanic panic fatal
        Level :"debug",
    },
    // 用戶
    User:[
        {
            User:"king",
            Password:"cerberus is an idea",
            Power:All,
        }
    ],
}