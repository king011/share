package speed

import (
	"container/list"
	"time"
)

// Node .
type Node struct {
	N int64
	T time.Time
}

// Statistics 統計下載速度
type Statistics struct {
	l        *list.List
	duration time.Duration
	last     time.Time
	cache    int64
}

// NewStatistics 創建 統計器
func NewStatistics(duration time.Duration) *Statistics {
	if duration < time.Second {
		duration = time.Second * 5
	}
	return &Statistics{
		l:        list.New(),
		duration: duration,
	}
}

// Speed 返回 每秒下載速度
func (s *Statistics) Speed() (speed int64) {
	now := time.Now()
	if !s.last.IsZero() && s.last.Add(time.Millisecond*100).After(now) {
		speed = s.cache
		return
	}

	if s.l.Len() == 0 {
		return
	}

	element := s.l.Front()
	node := element.Value.(Node)
	speed += node.N
	begin := node.T
	for element = element.Next(); element != nil; element = element.Next() {
		node := element.Value.(Node)
		speed += node.N
	}
	speed *= int64(time.Second)
	speed /= int64(now.Sub(begin))
	s.cache = speed
	s.last = now
	return
}

// Push 加入 記錄
func (s *Statistics) Push(n ...int64) {
	s.CheckTimeout()

	var sum int64
	for i := 0; i < len(n); i++ {
		sum += n[i]
	}
	if sum != 0 {
		s.l.PushBack(Node{
			N: sum,
			T: time.Now(),
		})
	}
}

// CheckTimeout 移除 超時的 項目
func (s *Statistics) CheckTimeout() {
	l := s.l
	if l.Len() == 0 {
		return
	}

	now := time.Now()
	element := l.Front()
	for element != nil {
		node := element.Value.(Node)
		if node.T.Add(s.duration).After(now) {
			break
		}
		// 刪除 超時 項目
		next := element.Next()
		l.Remove(element)
		element = next
	}
}
