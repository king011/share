package main

import (
	"gitlab.com/king011/share/cmd"
	"log"
)

func main() {
	if e := cmd.Execute(); e != nil {
		log.Fatalln(e)
	}
}
