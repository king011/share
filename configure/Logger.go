package configure

import (
	"os"
	"path/filepath"
	"strings"
)

// Logger 日誌 配置
type Logger struct {
	// 日誌 http 如果爲空 則不啓動 http
	HTTP string `json:"HTTP"`
	// 日誌 檔案名
	Filename string
	// 單個日誌檔案 大小上限 MB
	MaxSize int
	// 保存 多少個 日誌 檔案
	MaxBackups int
	// 保存 多少天內的 日誌
	MaxAge int
	// 要 保存的 日誌 等級 debug info warn error dpanic panic fatal
	Level string
}

// Format .
func (l *Logger) Format() (e error) {
	l.HTTP = strings.TrimSpace(l.HTTP)
	l.Filename = strings.TrimSpace(l.Filename)
	l.Level = strings.TrimSpace(l.Level)

	if l.Filename == "" {
		l.Filename = filepath.Dir(os.Args[0]) + "/logs/server.log"
	} else if !filepath.IsAbs(l.Filename) {
		l.Filename = filepath.Dir(os.Args[0]) + "/" + l.Filename
	}
	l.Filename, e = filepath.Abs(l.Filename)
	if e != nil {
		return
	}

	if l.MaxSize < 1 {
		l.MaxSize = 100
	}
	if l.MaxBackups < 1 {
		l.MaxBackups = 3
	}
	if l.MaxAge < 1 {
		l.MaxAge = 28
	}
	if l.Level == "" {
		l.Level = "warn"
	}
	return
}
