package configure

import (
	"strings"
)

// GRPC 服務配置
type GRPC struct {
	Addr     string
	CertFile string
	KeyFile  string
}

// Format .
func (g *GRPC) Format() (e error) {
	g.Addr = strings.TrimSpace(g.Addr)
	g.CertFile = strings.TrimSpace(g.CertFile)
	g.KeyFile = strings.TrimSpace(g.KeyFile)
	return
}
