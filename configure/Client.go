package configure

import (
	"encoding/json"
	"github.com/google/go-jsonnet"
	"io/ioutil"
	"strings"
)

// Client 客戶端 配置
type Client struct {
	// 服務器 地址
	Addr string
	// 是否使用 TLS 安全 傳輸
	TLS bool `json:"TLS"`
	// 是否 忽略 證書驗證
	SkipVerify bool

	// 用戶名 密碼
	User     string
	Password string
}

// Format 格式化 參數
func (c *Client) Format() (e error) {
	c.Addr = strings.TrimSpace(c.Addr)
	return
}
func (c *Client) String() string {
	if c == nil {
		return "nil"
	}
	b, e := json.MarshalIndent(c, "", "\t")
	if e != nil {
		return e.Error()
	}
	return string(b)
}

// Load 加載 配置
func (c *Client) Load(filename string) (e error) {
	var b []byte
	b, e = ioutil.ReadFile(filename)
	if e != nil {
		return
	}
	vm := jsonnet.MakeVM()
	var jsonStr string
	jsonStr, e = vm.EvaluateSnippet("", string(b))
	if e != nil {
		return
	}
	b = []byte(jsonStr)
	e = json.Unmarshal(b, c)
	if e != nil {
		return
	}
	e = c.Format()
	return
}
