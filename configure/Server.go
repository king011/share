package configure

import (
	"encoding/json"
	"github.com/google/go-jsonnet"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
)

// Server 服務器 配置
type Server struct {
	GRPC   GRPC `json:"GRPC"`
	Logger Logger
	Root   string
	User   []User
}

// Format 格式化 參數
func (s *Server) Format() (e error) {
	if e = s.GRPC.Format(); e != nil {
		return
	}
	if e = s.Logger.Format(); e != nil {
		return
	}
	s.Root = strings.TrimSpace(s.Root)
	if s.Root == "" {
		s.Root = "root"
	} else if !filepath.IsAbs(s.Root) {
		s.Root = filepath.Dir(os.Args[0]) + "/" + s.Root
	}
	s.Root, e = filepath.Abs(s.Root)
	return
}
func (s *Server) String() string {
	if s == nil {
		return "nil"
	}
	b, e := json.MarshalIndent(s, "", "\t")
	if e != nil {
		return e.Error()
	}
	return string(b)
}

// Load 加載 配置
func (s *Server) Load(filename string) (e error) {
	var b []byte
	b, e = ioutil.ReadFile(filename)
	if e != nil {
		return
	}
	vm := jsonnet.MakeVM()
	var jsonStr string
	jsonStr, e = vm.EvaluateSnippet("", string(b))
	if e != nil {
		return
	}
	b = []byte(jsonStr)
	e = json.Unmarshal(b, s)
	if e != nil {
		return
	}
	return
}
