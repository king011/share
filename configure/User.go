package configure

const (
	// PowerRead 允許 下載 檔案
	PowerRead = 0x1
	// PowerWrite 允許 上傳 檔案
	PowerWrite = 0x2
	// PowerAdmin 允許 管理服務器
	PowerAdmin = 0x4
)

// User 用戶 配置
type User struct {
	User     string
	Password string
	Power    uint32
}
