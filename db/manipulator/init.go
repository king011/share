package manipulator

import (
	"gitlab.com/king011/king-go/os/fileperm"
	"gitlab.com/king011/share/configure"
	"gitlab.com/king011/share/logger"
	"go.uber.org/zap"
	"log"
	"os"
)

var _Root string

// Init 初始化
func Init(root string, users []configure.User) (e error) {
	_Root = root
	e = os.MkdirAll(_Root, fileperm.Directory)
	if e != nil {
		logger.Fatal("init fatal",
			zap.Error(e),
		)
		return
	}
	logger.Info("root",
		zap.String("dir", _Root),
	)
	log.Println("root :", _Root)

	e = _User.Init(users)
	if e != nil {
		logger.Fatal("init fatal",
			zap.Error(e),
		)
		return
	}
	return
}

// Root 返回 共享根路徑
func Root() string {
	return _Root
}
