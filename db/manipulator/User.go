package manipulator

import (
	"crypto/sha512"
	"encoding/hex"
	"gitlab.com/king011/share/configure"
	"strings"
	"sync"
)

// User 用戶管理
type User struct {
	keys map[string]configure.User
	sync.Mutex
}

var _User User

// SingleUser 返回 用戶 單例
func SingleUser() *User {
	return &_User
}

// Init 重設 用戶
func (m *User) Init(users []configure.User) (e error) {
	keys := make(map[string]configure.User)
	for i := 0; i < len(users); i++ {
		users[i].User = strings.TrimSpace(users[i].User)
		data := sha512.Sum512([]byte(users[i].Password))
		users[i].Password = hex.EncodeToString(data[:])
		keys[users[i].User] = users[i]
	}

	m.Lock()
	m.keys = keys
	m.Unlock()
	return
}

// Get 返回 用戶
func (m *User) Get(name string) (user *configure.User) {
	m.Lock()
	if find, ok := m.keys[name]; ok {
		user = &configure.User{
			User:     find.User,
			Password: find.Password,
			Power:    find.Power,
		}
		m.Unlock()
		return
	}
	m.Unlock()
	return
}
