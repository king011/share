package logger

import (
	"gitlab.com/king011/share/configure"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/natefinch/lumberjack.v2"
	"log"
	"net"
	"net/http"
)

// Init 初始化 日誌 單件
func Init(cnf *configure.Logger) {
	// 創建 日誌 輸出 檔案
	w := zapcore.AddSync(&lumberjack.Logger{
		Filename:   cnf.Filename,
		MaxSize:    cnf.MaxSize, // megabytes
		MaxBackups: cnf.MaxBackups,
		MaxAge:     cnf.MaxAge, // days
	})

	// 創建 core
	atom := zap.NewAtomicLevel()
	switch cnf.Level {
	case "debug":
		atom.SetLevel(zapcore.DebugLevel)
	case "info":
		atom.SetLevel(zapcore.InfoLevel)
	case "warn":
		atom.SetLevel(zapcore.WarnLevel)
	case "error":
		atom.SetLevel(zapcore.ErrorLevel)
	case "dpanic":
		atom.SetLevel(zapcore.DPanicLevel)
	case "panic":
		atom.SetLevel(zapcore.PanicLevel)
	case "fatal":
		atom.SetLevel(zapcore.FatalLevel)
	}
	encoderCfg := zap.NewProductionEncoderConfig()
	encoderCfg.EncodeCaller = CallerEncoder
	core := zapcore.NewCore(
		zapcore.NewJSONEncoder(encoderCfg),
		w,
		atom,
	)

	// 創建 記錄器
	_Logger = zap.New(core,
		zap.AddCaller(), // 需要 輸出 代碼 位置
		zap.AddCallerSkip(1),
	)

	if cnf.HTTP != "" {
		// 運行 http 服務
		srv := http.Server{Handler: atom}
		l, e := net.Listen("tcp", cnf.HTTP)
		if e == nil {
			log.Println("zap http running", cnf.HTTP)
			_Logger.Info("zap http running",
				zap.String("addr", cnf.HTTP),
			)
			go srv.Serve(l)
		} else {
			_Logger.Error("zap http error",
				zap.Error(e),
			)
		}
	}
}
